<?php

define('SH_MARXUP', true);

// PHP sessions are only truly expired when the garbage collector gets to them.
// This bit starts the session and ensures that "dead" sessions can't be used.
session_start();
if(isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY']) < 1800){
	$_SESSION['LAST_ACTIVITY'] = time();
}else{
	foreach($_SESSION as &$x){
		$x = false;
	}
	$_SESSION['LAST_ACTIVITY'] = time();
}

// Makes a lot of Errors catchable; Dispatcher relies on this and this is better anyway
set_error_handler(function($errno, $errstr, $errfile, $errline ){
    throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
});

// Load up everything we need, define a few globals from singleton classes so we don't
// waste CPU power reinstancing these.
require_once('./config.php');
require_once('./core/lib/loader.php');
$db = new db();
$u = new User();
$auth = new Auth();

require('./dispatcher.php');