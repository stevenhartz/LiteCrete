# LiteCrete

A lightweight php CMS inspired by Concrete5. A lot of the nicer features Concrete5 provides are missing, and will
stay missing; for starters, the one-man dev team at LiteCrete can't hope to compete with that. For another,
they add a lot if complexity that I'm hoping to avoid. The goal here is keep the baseline a pretty small set
of code so that additions can be made wherever they need to be made by an end user.

## Features

### Security

I hope that, through simplicity, it will be easy to keep things secure. In addition, the follow features
should help improve LiteCrete's security and help reduce end-user security difficulties:
  * PDO library for parameterized MySQL queries. (Protection against SQL injection)
  * Auth class provides nonce management (Protection against CSRF if used)
  * User class employs Argon2 password algorithm with automatic salt management (PHP implements most of that for me)
  * 10-character password requirement, including all special characters.
  * Advanced session management

### Flexibility

It doesn't get more flexible than this. Due to the simplicity of the baseline code, it's easy to find out where
code needs changing. Apps are straightforward to implement, and resemble Concrete5's single pages including
RESTful URL mapping. LiteCrete includes just enough scaffolding to make jumping into a new app fast and security
a breeze.

## Page-Load Flow

Incoming requests are handled by index.php. This drags in /core/lib/loader.php, which brings the default models,
and kicks off dispatcher.php. The Dispatcher class defined (and instanced) in dispatcher.php will then pull in
whatever (M)VC it needs to complete the request, call the appropriate Controller method, and launch the View.

```
index.php -> config.php
          -> loader.php
                        -> user.php (User model)
                        -> db.php   (Database model)
                        -> auth.php (Authentication helpers/model)
                           ...
          -> dispatcher.php
                            -> [App Controller]
                            -> [App View]
```

## Making a new App

Create a new folder in /apps with the name of your application. Then add a class that extends CoreApp,
add an HTML-based view.php file, and you're off to the races. The posts app has been left in the user-app
directory so you can see an example of how simple an app can be. There's nothing stopping you from getting
quite complex, however! 

/apps/showuser/controller.php
```language=php
class ShowuserApp extends CoreApp{
	function run(){
		global $u;
		$this->set('display_name', $u->getDisplayName());
	}
}
```

/apps/showuser/view.php
```language=php
<html>
  <body>
	<p>Welcome, <?php echo $this->get('display_name') ?>!</p>
  </body>
</html>
```

Check out the documentation for a less contrived example!

## Requirements

LiteCrete is known to work on a system with:
  * Ubuntu 16.04 LTS
  * Apache 2
  * PHP 7.2 (Requires 7.2 for Argon2 support)
  * MySQL 5

## To-dos:

  * Convert nonce-functions to static (auth.php)
  * Passwords: disallow common passwords
  * SQL scripts for setup
  * PHP setup
  * Fix bugs.
