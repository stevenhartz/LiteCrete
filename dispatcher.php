<?php
defined('SH_MARXUP') or die();

/*
  Handles routing
  
  Notable routes:
    404
    admin
    post
    edit
*/

class Dispatcher{

	const REQUIRE_BASE = './apps/';
	private $base_path = false;
	private $controller_path = false;
	private $view_path = false;

	function do404(){
		header("HTTP/1.0 404 Not Found");
		$path_404 = self::REQUIRE_BASE . '404.php';
		if(file_exists($path_404)){
			require($path_404);
		}else{
			echo 'File not found.';
		}
		die();
	}

	function __construct(){
		// Strip off '?' parameters because they confuse everything.
		$full_uri = explode('?', $_SERVER['REQUEST_URI']);
		$path = explode('/', $full_uri[0]);
		
		// Catches trailing slash
		if(count($path) > 2 && $path[count($path)-1] == '')
			unset($path[count($path)-1]);
			
		// Dashes are pretty but illegal in class names; this converts dashes in
		// paths to underscores for back-end code.
		$path = str_replace('-', '_', $path);
			
		switch($path[1]){
		case '..':
		case '404':
			self::do404();
			return;
			break;
		
		case 'admin':
			$this->base_path = './core/apps/admin/';
			$classname = 'AdminApp';
			break;
			
		case '':
			$app_path = 'home';
			$classname = 'HomeApp';
			break;
			
		default:
			$app_path = strtolower($path[1]);
			$classname = strtoupper($path[1][0]) . strtolower(substr($path[1],1)) . 'App';
		}
		
		if($this->base_path === false){
			$this->base_path = self::REQUIRE_BASE . $app_path;
		}
		$this->controller_path =  $this->base_path . '/controller.php';
		
		if(file_exists($this->controller_path)){
			require_once($this->controller_path);
			try{
				$this->controller = new $classname;
			}catch(ErrorException $e){
				$this->controller = new CoreApp();
			}
			
			if(count($path) == 2){
				$this->controller->run();
			}else{
				if(method_exists($this->controller, $path[2])){
					call_user_func_array(array($this->controller, $path[2]), array_slice($path, 3));
				}else
					self::do404();
			}
			
			// This is here to let a controller specify a different view. The admin app uses
			// this for /admin/add-user, for instance
			$this->view_path = trim($this->base_path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $this->controller->view();
			
			// the code in view_path gets run in this scope, do without
			// these global definitions, view.php doesn't get globals.
			global $auth;
			require($this->view_path);
		}else{
			self::do404();
		}
	}
	
	function get_inc_url(){
		if($this->controller_path === false){
			return '/';
		}
		
		// Remove leading '.' and we're set.
		return substr($this->controller_path, 1);
	}
	
	// Dispatcher::inc_style($source, $arg1, $arg2=NULL)
	//   $source    "core", "common", or an app name.
	//   $arg1      If $source is not core, this is the file to include
	//              If $source is core, this is the app within admin to check.
	//   $arg2      If $source is not core, this is ignored.
	//              If $source is core, this is the file to include.
	//
	// Returns the <link> tag needed to include the CSS file.
	// Examples:
	// $this->inc_style('my_app','style.css')
	//    <link rel="stylesheet" href="/apps/my-app/style.css />
	// $this->inc_style('common','master-style.css')
	//    <link rel="stylesheet" href="/common/master-style.css" />
	// $this->inc_style('complex_app', 'styles/thursday/style-on-thursdays.css')
	//    <link rel="stylesheet" href="/apps/complex-app/styles/thursday/style-on-thursdays.css" />
	// $this->inc_style('core', 'admin', 'style.css')
	//    <link rel="stylesheet" href="/core/apps/admin/style.css" />
	function inc_style($source, $arg1, $arg2=NULL){
		switch($source){
			case 'core':
				if(is_null($arg2))
					throw ErrorException('Dispatcher::inc_style() requires 3 arguments when $source is "core"');
				$inc_url = '/core/apps/'.$arg1.'/'.$arg2;
				break;
			case 'common':
				$inc_url = '/common/' . $arg1;
				break;
			default:
				$inc_url = '/apps/' . $source . '/' . $arg1;
				break;
		}
		
		return '<link rel="stylesheet" href="' . $inc_url .'" />';
	}
	
	// Meant to help include commonly used elements in views (like headers and footers)
	// Uses and include to dump code in-place. The $source parameter is local to the view
	// which calls. (This is a convenience function)
	function inc_html($source){
		$path = trim($this->base_path,DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $source;
		include($path);
	}

}

$dispatcher = new Dispatcher();