<?php

defined('SH_MARXUP') or die('Access Denied');

class Group{

	protected $id = false;
	protected $name = '';
	
	static function from_id($id){
		global $db;
		
		$g = new Group();
		
		$group = $db->query('SELECT * FROM Groups WHERE groupID=? LIMIT 1', array($id));
		if(count($group) != 1){
			return false;
		}else{
			$g->id = $id;
			$g->name = $group[0]['group_name'];
			return $g;
		}
	}
	
	static function from_name($name){
		global $db;
		
		$g = new Group();
		
		$group = $db->query('SELECT * FROM Groups WHERE group_name=? LIMIT 1', array($name));
		if(count($group) != 1){
			return false;
		}else{
			$g->id = $group[0]['groupID'];
			$g->name = $name;
			return $g;
		}
	}
	
	static function get_all(){
		global $db;
		
		$groups_raw = $db->query('SELECT * FROM Groups');
		$groups = array();
		
		foreach($groups_raw as $group_raw){
			$g = new Group();
			$g->id = $group_raw[0]['groupID'];
			$g->name = $group_raw[0]['group_name'];
			$groups[] = $g;
		}
		
		return $groups;
	}
	
	function contains_user(User $user){
	
		if($this->id === false)
			return false;
		
		global $db;
		$result = $db->query('SELECT * from user_group_map WHERE userID=? and groupID=?', array($user->getUserID(), $this->id));
		if(count($result)==1){
			return true;
		}
		return false;
	}
	
	
	
	

}