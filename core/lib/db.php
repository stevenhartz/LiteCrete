<?php

defined('SH_MARXUP') or die();

class db{

	private $dbh = false;

	function __construct(){
		try{
			$this->dbh = new PDO('mysql:host=localhost;dbname='.SH_DB, SH_DB_U, SH_DB_P);
		} catch (PDOException $e) {
			$this->dbh = false;
		}
	}
	
	function isOK(){
		return $this->dbh !== false;
	}

	function query($qstr, array $arguments=array()){
	
		if($this->isOK()){
			$stmt = $this->dbh->prepare($qstr);
			$stmt->execute($arguments);
			return $stmt->fetchAll();
		}else{
			throw Error("Not connected to database");
		}
	}

	function insertID(){
		return $dbh->lastInsertId();
	}
}
