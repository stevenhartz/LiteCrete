<?php
defined('SH_MARXUP') or die();

class CoreApp{

	protected $values = array();
	protected $view = 'view';

	function run() { }
	
	protected function set($name, $value){
		$this->values[$name] = $value;
	}
	
	function get($name){
		if(array_key_exists($name, $this->values))
			return $this->values[$name];
		else
			return NULL;
	}
	
	// !! This should not be reimplemented by extending classes !!
	function view($view=NULL){
		if(is_null($view)){
			return $this->view.'.php';
		}
		return $view.'.php';
	}

}