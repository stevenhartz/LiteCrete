<?php

defined('SH_MARXUP') or die('Access Denied');

class Auth{

	const max_pw_len = 255;
	const min_pw_len = 10;
	const nonce_size = 64;
	const nonce_life = 15*60; // 15 minutes
	
	// Used by put_nonce. This name should be unlikely to cause collisions
	// with other HTML form elements.
	const input_name = 'lc_nonce';

	function get_max_pw_len(){
		return self::max_pw_len;
	}

	function get_min_pw_len(){
		return self::min_pw_len;

	}

	function authenticate($submitted_pw, $database_hash){
		$database_hash = base64_decode($database_hash);
		return password_verify($submitted_pw, $database_hash);
	}

	function hash($password){
		// Overly-long passwords can be used for DDOS attacks, so we'll limit PWs
		if(strlen($password) > self::max_pw_len){
			throw new Error('Password too long');
		}elseif(strlen($password) < self::min_pw_len){
			throw new Error('Password too short');
		}

		// password_hash handles salting automatically, do not add your own!
		$ph = password_hash($password, PASSWORD_ARGON2I);
		return base64_encode($ph);
	}
	
	// Generates a new nonce, formats an <input type="hidden"> for it, and prints it.
	// Auth::check_nonce will check this post variable for data is no argument.
	// This is the easiest way to get a nonce on the page.
	function put_nonce(){
		$nonce = $this->get_nonce();
		echo '<input type="hidden" name="' . self::input_name .'" value="' . $nonce . '" />';
	}
	
	// Generates a new nonce and base64 encodes it; does not output anything.
	function get_nonce(){
		global $db;
		$nonce = base64_encode(random_bytes(self::nonce_size));
		
		$db->query('INSERT INTO `Nonces` (`id`, `nonce_64`, `expire`) VALUES (NULL, ?, ?);', array($nonce, time() + self::nonce_life));
		
		return $nonce;
	}
	
	// If provided an argument, checks if the value is a currently valid nonce. If it is,
	// it's marked as expired and true is returned. If it's invalid, returns false.
	// With no argument, this function checks the $_POST array for a nonce from put_nonce().
	// If this nonce is valid, returns true and invalidates the nonce.
	function check_nonce($nonce=NULL){
		global $db;
		
		if(is_null($nonce)){
			if(isset($_POST[self::input_name]))
				$nonce = $_POST[self::input_name];
			else
				return false;
		}
		
		$result = $db->query('SELECT * FROM Nonces WHERE nonce_64=? AND expire>? LIMIT 1', array($nonce, time()));
		
		if(count($result) == 1){
			// Force this nonce to be expired
			$db->query('UPDATE Nonces SET expire=0 WHERE id=?', array($result[0]['id']));
			return true;
		}
		return false;
	}
}
