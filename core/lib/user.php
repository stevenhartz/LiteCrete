<?php

defined('SH_MARXUP') or die('Access Denied');

class User{

	protected $id = false;
	protected $username = '';
	protected $displayName = '';
	
	function __construct(){
		global $db;
		if(isset($_SESSION['userID'])){
			$userInfo = $db->query('SELECT * from Users where userID=? LIMIT 1', array($_SESSION['userID']));
			if(count($userInfo) != 1){
				return;
			}
			$userInfo = $userInfo[0];
			$this->id = $userInfo['userID'];
			$this->username = $userInfo['username'];
			$this->displayName = $userInfo['display_name'];
		}else{
			//echo "Not attempting to log you back in.<br />\n";
			//print_r($_SESSION);
		}
	}
	
	function getUserId(){
		return $this->id;
	}
	
	function getUsername(){
		return $this->username;
	}
	
	function getDisplayName(){
		return $this->displayName;
	}
	
	function isLoggedIn(){
		return $this->id !== false;
	}
	
	function logout(){
		$this->id = false;
		$this->username = '';
		$this->displayName = '';
		unset($_SESSION['userID']);
	}
	
	function login($un, $pw){
		global $db;
		
		if($this->isLoggedIn()){
			throw Error("User already logged in!");
		}
		
		$userInfo = $db->query('SELECT * from Users where username=? LIMIT 1', array($un));
		if(count($userInfo) != 1){
			return false;
		}else{
			$userInfo = $userInfo[0];
			$a = new Auth();
			
			if($a->authenticate($pw, $userInfo['password'])){
				$this->id = $userInfo['userID'];
				$this->username = $userInfo['username'];
				$this->displayName = $userInfo['display_name'];
				$_SESSION['userID'] = $this->id;
				
				// This prevents anyone with an old session cookie from accessing the new logged-in session.
				session_regenerate_id();
				
				return true;
			}else{
				return false;
			}
		}
	}
	
	function changePassword($current_pw, $new_pw){
		global $db;
		
		if($this->isLoggedIn()){
			// Set new password only if old password was correct
			$userInfo = $db->query('SELECT * from Users where userID=? LIMIT 1', array($this->id));
			if(count($userInfo) == 1){
				$a = new Auth();
				if($a->authenticate($current_pw, $userInfo['password'])){
					$new_pw_hash = $a->hash($new_pw);
					$db->query('INSERT INTO Users SET password=? WHERE userID=?', array($new_pw_hash, $this->id));
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

}