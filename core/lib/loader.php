<?php
/**********

Designates library files that should be loaded. This keeps
index.php clean by handing this tast off to this file.

**********/

require_once('db.php');
require_once('user.php');
require_once('auth.php');
require_once('post.php');
require_once('app.php');
require_once('group.php');