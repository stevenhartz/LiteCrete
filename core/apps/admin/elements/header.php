<?php defined('SH_MARXUP') or die(); ?>
<html>
	<head>
		<?php echo $this->inc_style('common', 'bootstrap.min.css') ?>
		<?php echo $this->inc_style('core', 'admin', 'style.css') ?>
	</head>
	<body>
		<div id="header" class="admin">
			<div id="user">You're logged in as <span class="user"><?php echo $this->controller->get('user') ?></span></div>
			<div id="functions">
				<?php
					if(is_array($this->controller->get('functions'))){
						echo '<ul class="menu func-menu">';
						foreach($this->controller->get('functions') as $func){
							echo '<li class="function"><a href="' . $this->get_inc_url() . '/' . $func . '">' . ucwords(str_replace(array('-','_'),' ', $func)) . '</li>';
						}
						echo '</ul>';
					}
				?>
			</div>
			<div id="logout">
				<a href="/logout" class="btn btn-danger">Log out</a>
			</div>
		</div>
		<div id="content" class="admin">