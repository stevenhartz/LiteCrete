<?php defined('SH_MARXUP') or die();

class AdminApp extends CoreApp{

	function __construct(){
		global $u;
		if(!$u->isLoggedIn()){
			header('Location: /home');
			die();
		}
		
		// These should be included in every request.
		$this->set('user', $u->getDisplayName());
		$this->set('functions', array('add-user', 'write-post'));
	}

	function run(){
		global $db;
		
		// Fetch all users, combine with the posts table and count number of posts with each user.
		// It took some fiddling in PhpMyAdmin to get this right. You need the userID for GROUP BY
		// to be valid.
		$q = 'SELECT Users.userID, Users.username as name, COUNT(Posts.postID) numPosts FROM Users LEFT OUTER JOIN Posts ON Posts.ownerID=Users.userID GROUP BY Users.userID';
		
		$this->set('users', $db->query($q));
	}
	
	function add_user(){
		global $db;
		global $u;
		
		// Ensure current user is in the Admin group or reject what they're trying to do.
		$admins = Group::from_name('Admins');
		if(!$admins->contains_user($u)){
			echo "You don't have access to this function. Head back to the <a href='/admin'>admin page</a>";
			die();
		}
		
		$this->set('verb','add');
		
		if(isset($_POST['add-user'])){
			$this->set('add-attempt', true);
			
			$a = new Auth();
			
			if(!$a->check_nonce()){
				$this->set('add-success', false);
				return;
			}else{
				$reasons = array();
				if($_POST['password'] != $_POST['cpassword']){
					$this->set('add-success', false);
					$reasons[] = 'The passwords must match.';
				}
				if(strlen($_POST['password']) < Auth::min_pw_len){
					$this->set('add-success', false);
					$reasons[] =  'The password must be at least ' . Auth::min_pw_len . ' characters.';
				}
				if(strlen($_POST['password']) > Auth::max_pw_len){
					$this->set('add-success', false);
					$reasons[] =  'The password must be no more than ' . Auth::max_pw_len . ' characters. (It would be darn hard to guess, though.)';
				}
				//We don't need to make database requests unless we think that everything else is ok.
				if(count($reasons) == 0){
					$hits = $db->query('SELECT * FROM Users where username=? LIMIT 1', array($_POST['username']));
					if(count($hits) != 0){
						$this->set('add-success', false);
						$reasons[] = 'That username is already in use';
					}else{
						$pwh = $a->hash($_POST['password']);
						$db->query('INSERT INTO Users (userID, username, display_name, password) VALUES (NULL, ?, ?, ?)', array($_POST['username'], $_POST['display_name'], $pwh));
						$this->set('add-success', true);
					}
					
				}
				$this->set('reasons',$reasons);
			}
		}else{
			$this->set('add-attempt',false);
		}
		
		$this->view = 'add_edit';
	}
	
	function edit_user($id){
		$this->view = 'add_edit';
	}

}