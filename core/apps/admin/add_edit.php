<?php defined('SH_MARXUP') or die();

$this->inc_html('elements/header.php');
	
	$restore_post = false;
	if($this->controller->get('add-attempt')){
		if($this->controller->get('add-success') == true){
			echo '<div class="alert alert-success">User '.$this->controller->get('verb').'ed!</div>';
		}else{
			$restore_post = true;
			echo '<div class="alert alert-danger"><ul>';
			foreach($this->controller->get('reasons') as $reason){
				echo '<li>' . $reason . '</li>';
			}
			echo '</ul></div>';
		}
	}
	
	?>
	
	<form action="" method="post">
		<input type="text" name="display_name" placeholder="New Display Name" <?php if($restore_post) echo "value='{$_POST['display_name']}'"?> />
		<input type="text" name="username" placeholder="New Username" <?php if($restore_post) echo "value='{$_POST['username']}'"?> />
		<input type="password" name="password" placeholder="Password" />
		<input type="password" name="cpassword" placeholder="Confirm Password" />
		<input type="submit" class="btn btn-primary" name="add-user" value="Add user" />
		<a href="/admin" class="btn btn-danger">Cancel</a>
		<?php $auth->put_nonce(); ?>
	</form>
	<?php
	
$this->inc_html('elements/footer.php');