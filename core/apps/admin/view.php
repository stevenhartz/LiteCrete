<?php defined('SH_MARXUP') or die();

$this->inc_html('elements/header.php');
			if(!is_null($this->controller->get('users'))){
				?>
				<table class="table users">
					<thead>
						<tr><td>User (Display Name)</td><td>Number of Posts</td></tr>
					</thead>
					<tbody>
						<?php
						if(is_array($this->controller->get('users'))){
							foreach($this->controller->get('users') as $user){
								echo '<tr><td>' . $user['name'] . '</td><td>' . $user['numPosts'] .'</td></tr>';
							}
						}
						?>
					</tbody>
					<tfoot>
						<tr><td colspan="2"><a href="/admin/add-user" class="btn btn-primary add-user"><span class="glyphicon glyphicon-plus"></span>Add User</a></td></tr>
					</tfoot>
				</table>
				<?php
			}
$this->inc_html('elements/footer.php');