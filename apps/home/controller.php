<?php

defined('SH_MARXUP') or die('Access Denied');

class HomeApp extends CoreApp{
	
	function run(){
		global $u;
		$this->set('myparam','<b><i>DYNAMIC CONTENT!!!</i></b>');
		if($u->isLoggedIn()){
			$this->set('logged_in',true);
			$this->set('display_name',$u->getDisplayName());
		}else{
			$this->set('logged_in',false);
		}
	}
	
	function say_hi(){
		die("Hello!");
	}
}