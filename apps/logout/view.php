<?php defined('SH_MARXUP') or die(); ?>
<html>
	<body>
		<?php $loggedOut = $this->controller->get('logged_out'); if(!is_null($loggedOut) && $loggedOut === true){ ?>
		<p>You were logged out. <a href="/">Return home</a></p>
		<?php }else{ ?>
		<p>Something went wrong logging you out. You should probably try again?</p>
		<?php } ?>
	</body>
</html>