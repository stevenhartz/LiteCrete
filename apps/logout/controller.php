<?php defined('SH_MARXUP') or die();

class LogoutApp extends CoreApp{
	
	function run(){
		global $u;
		
		$u->logout();
		$this->set('logged_out', true);
	}
}