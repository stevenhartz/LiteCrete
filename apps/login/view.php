<html>
	<head>
		
	</head>
	<body>
		<div id="wrapper">
			<?php if($this->controller->get('login_failed')) { ?>
			<div class="warning">That wasn't quite right. Try again.</div>
			<?php }?>
			
			<form action="/login" method="post">
				<input type="text" name="user" placeholder="Username" />
				<input type="password" name="password" placeholder="Password" />
				<input type="submit" value="Log in" name="submit" />
				<input type="hidden" name="nonce" value="<?php $auth = new Auth(); echo $auth->get_nonce(); ?>" />
			</form>
		</wrapper>