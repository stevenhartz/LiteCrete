<?php

defined('SH_MARXUP') or die();

class LoginApp extends CoreApp{
	
	function run(){
		global $auth;
		global $u;
		
		if(isset($_POST['submit'])){
			if(!isset($_POST['nonce']))
				return;
			$nonce = $_POST['nonce'];
			if(!$auth->check_nonce($nonce)){
				echo "nonce failed<br />\n";
				return;
			}
			
			if(!(isset($_POST['user']) && isset($_POST['password'])))
				return;
				
			if($u->login($_POST['user'], $_POST['password'])){
				header('Location: /admin');
			}else{
				$this->set('login_failed', true);
			}
		}
	}
}